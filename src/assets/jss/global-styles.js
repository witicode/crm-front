/*!

 =========================================================
 *Global Styles - v1.0.0
 =========================================================

*/

// ##############################
// // // Function that converts from hex color to rgb color
// // // Example: input = #9c27b0 => output = 156, 39, 176
// // // Example: input = 9c27b0 => output = 156, 39, 176
// // // Example: input = #999 => output = 153, 153, 153
// // // Example: input = 999 => output = 153, 153, 153
// #############################

// ##############################
// // // Variables - Styles that are used on more than one component
// #############################

import { primaryColor, grayColor, dangerColor } from "assets/jss/material-dashboard-react.js";

//const drawerWidth = 260;

// start of select box styles

const dot = (color = '#ccc') => ({
  alignItems: 'center',
  display: 'flex',

  ':before': {
    backgroundColor: color,
    borderRadius: 10,
    content: '" "',
    display: 'block',
    marginLeft: 8,
    height: 10,
    width: 10,
  },
});

const selectStyles = {
  control: (styles) => {

    return {
      ...styles,
      backgroundColor: 'white',

      ':hover': {
        ...styles[':hover'],
        boxShadow: `0 0 1px ${primaryColor[0]}`,
        borderColor: primaryColor[0]
      }
    };
  },
  option: (styles, { data, isDisabled, isFocused, isSelected }) => {

    return {
      ...styles,
      backgroundColor: isDisabled
        ? null
        : isSelected
        ? primaryColor[0]
        : isFocused
        ? grayColor[11]
        : null,
      color: 'black',
      cursor: isDisabled ? 'not-allowed' : 'default',

      ':active': {
        ...styles[':active'],
        backgroundColor:
          !isDisabled && (isSelected ? primaryColor[0] : primaryColor[1]),
        boxShadow: `0 0 1px ${primaryColor[0]}`,
      },
    };
  },
  input: styles => ({ ...styles, ...dot() }),
  placeholder: styles => ({ ...styles, ...dot() }),
  singleValue: (styles, { data }) => ({ ...styles, ...dot(primaryColor[0]) })
}

const errorSelectStyles  = {
  control: (styles, {isError}) => {

    return {
      ...styles,
      backgroundColor: 'white',

      borderColor: dangerColor[0],

      ':hover': {
        ...styles[':hover'],
        boxShadow: `0 0 1px ${dangerColor[0]}`,
        borderColor: primaryColor[0]
      }
    };
  },
  option: (styles, { data, isDisabled, isFocused, isSelected }) => {

    return {
      ...styles,
      backgroundColor: isDisabled
        ? null
        : isSelected
        ? primaryColor[0]
        : isFocused
        ? grayColor[11]
        : null,
      color: 'black',
      cursor: isDisabled ? 'not-allowed' : 'default',

      ':active': {
        ...styles[':active'],
        backgroundColor:
          !isDisabled && (isSelected ? primaryColor[0] : primaryColor[1]),
        boxShadow: `0 0 1px ${primaryColor[0]}`,
      },
    };
  },
  input: styles => ({ ...styles, ...dot() }),
  placeholder: styles => ({ ...styles, ...dot() }),
  singleValue: (styles, { data }) => ({ ...styles, ...dot(dangerColor[0]) })
}

// end of select box styles

export {
  //functions
  //variables
  //components
  selectStyles,
  errorSelectStyles
};
