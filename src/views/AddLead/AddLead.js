import React, {useState, useEffect} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles"
import { selectStyles, errorSelectStyles } from "assets/jss/global-styles.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import Box from '@material-ui/core/Box';

// custom components
import {useTranslation} from "react-i18next";
import Select from 'react-select';
import Creatable from 'react-select/creatable';
import { LeakAdd, Phone } from "@material-ui/icons";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  },
  selectWrapper: {
    marginTop: '20px'
  }
};

const useStyles = makeStyles(styles);

export default function UserProfile() {
  const [errorList, setErrorList] = useState({});
  const classes = useStyles();
  const {t, i18n} = useTranslation('common');

  const genders = [
    { value: 'male', label: t('CRM.male') },
    { value: 'female', label: t('CRM.female') },
    { value: 'other', label: t('CRM.other') }
  ]

  const countries = [
    { value: 'palestine', label: t('CRM.palestine') }
  ]

  const cities = [
    { value: 'nablus', label: t('CRM.nablus') },
    { value: 'Ramallah', label: t('CRM.ramallah') },
    { value: 'Bethlehem', label: t('CRM.bethlehem') },
    { value: 'Jenin', label: t('CRM.jenin') },
    { value: 'Hebron', label: t('CRM.hebron') },
    { value: 'Tulkarm', label: t('CRM.tulkarm') },
  ]

  const counties = [
    { value: 'Sara', label: t('CRM.sara') },
    { value: 'Tel', label: t('CRM.tel') },
    { value: 'Marda', label: t('CRM.marda') },
    { value: 'Beta', label: t('CRM.beta') }
  ]

  const lead = {
    firstName: null,
    secondName: null,
    thirdName: null,
    lastName: null,
    cellPhone: null,
    telephone: null,
    email: null,
    position: null,
    country: null,
    city: null,
    county: null,
    postalCode: null,
    gender: null
  }

  const addLead = event => {
    debugger;
    const newErrorList = errorList;
    if(!lead.gender) {
      errorList.gender = "required";
    }
    if(!lead.city) {
      newErrorList.city = "required";
    }

    setErrorList(newErrorList);
    console.log(errorList);
    debugger;
  };

  // setters
  const setGender = (e) => {
    lead.gender = e.value;
  }

  const setCity = (e) => {
    lead.city = e.value;
  }

  useEffect(() => {
    console.log('soso', errorList);
  })

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>{t('CRM.add-lead')}</h4>
            </CardHeader>
            <CardBody>
            <GridContainer>
                <GridItem xs={12} sm={6} md={3}>
                  <CustomInput
                    labelText={t('CRM.first-name')}
                    id="first-name"
                    formControlProps={{
                      fullWidth: true
                    }}
                    error={errorList.firstName}
                  />
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                  <CustomInput
                    labelText={t('CRM.second-name')}
                    id="second-name"
                    formControlProps={{
                      fullWidth: true
                    }}
                    error={errorList.secondName}
                  />
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                  <CustomInput
                    labelText={t('CRM.third-name')}
                    id="third-name"
                    formControlProps={{
                      fullWidth: true
                    }}
                    error={errorList.thirdName}
                  />
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                  <CustomInput
                    labelText={t('CRM.last-name')}
                    id="last-name"
                    formControlProps={{
                      fullWidth: true
                    }}
                    error={errorList.lastName}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={6} md={3}>
                  <CustomInput
                    labelText={t('CRM.position')}
                    id="position"
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                  <CustomInput
                    labelText={t('CRM.cell-phone')}
                    id="cell-phone"
                    formControlProps={{
                      fullWidth: true
                    }}
                    error={errorList.cellPhoneError}
                  />
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                  <CustomInput
                    labelText={t('CRM.telephone')}
                    id="telephone"
                    formControlProps={{
                      fullWidth: true
                    }}
                    error={errorList.telephone}
                  />
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                  <CustomInput
                    labelText={t('CRM.email-address')}
                    id="email-address"
                    formControlProps={{
                      fullWidth: true
                    }}
                    error={errorList.email}
                  />
                </GridItem>
              </GridContainer>
              
              <GridContainer>
                <GridItem xs={12} sm={6} md={3}>
                  <Box mt={2}>
                    <label>{t('CRM.country')}</label>
                    <Select options={countries} defaultValue={countries[0]} styles={selectStyles} isSearchable isError={false} />
                  </Box>
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                  <Box mt={2}>
                    {errorList.city}
                    <label >{t('CRM.city')}</label>
                    <Creatable
                      isClearable
                      isSearchable
                      options={cities}
                      styles={errorList.city ? errorSelectStyles : selectStyles}
                      onChange={setCity}
                    />
                  </Box>
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                  <Box mt={2}>
                    <label >{t('CRM.county')}</label>
                    <Creatable
                      isClearable
                      isSearchable
                      options={counties}
                      styles={selectStyles}
                    />
                  </Box>
                </GridItem>
                <GridItem xs={12} sm={6} md={3}>
                  <CustomInput
                    labelText={t('CRM.postal-code')}
                    id="postal-code"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      disabled: true
                    }}
                    error={errorList.postalCode}                 
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={6} md={3}>
                  <Box mt={2}>
                    <label >{t('CRM.gender')}</label>
                    <Select options={genders} defaultValue={genders[0]}
                            styles={errorList.gender ? errorSelectStyles : selectStyles}
                            onChange={setGender} />
                  </Box>
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter>
              <Button color="primary" onClick={addLead}>{t('Common.add')}</Button>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
